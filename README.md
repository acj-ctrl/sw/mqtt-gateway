MQTT-Gateway
============

This project acts as a gateway between the [Firware for Arduino Micro shield](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw) and a [MQTT broker](https://mqtt.org/), e.g. [Mosquitto](https://www.mosquitto.org/).  

## Installation
Since this gateway is written as a [Node-RED](https://nodered.org/) Flow it naturally requires an [installation of the Node-RED environment](https://nodered.org/docs/getting-started/). Fortunately this is supported on most operating systems and platform, e.g. the [Raspberry Pi OS](https://nodered.org/docs/getting-started/raspberrypi). 

## Usage
To use this Node-RED project load it in the Web GUI,  
change the Global Configuration Nodes for your mqtt-broker (called gimli in this project) and serial-port (/dev/ttyACM0:9600-8N1 in this project) to match your local settings.  
Then Deploy it.  
Now the ACJ-Controller shall connect (check the connection status for the Serial nodes) and start sending JSON messages.  
If configured correctly all the MQTT nodes should also connect to your broker and publish/subscribe to topics as described below

## MQTT topics
### publishing to
**home/acjCtrl/pwrCtrl/source** setPointSource (can be either "local" or "remote")  
**home/acjCtrl/pwrCtrl/setpoint** the currently used temperature setpoint \[deg C]  
**home/acjCtrl/pwrCtrl/temp** this is the measured room temperature \[deg C]  
**home/acjCtrl/pwrCtrl/power** the heating power \[kW]  
**home/acjCtrl/pwrCtrl/pidReqPower** the power that the PID control algorithm requests \[kW]  
**home/acjCtrl/pwrCtrl/activePhases** shows which phases are enabled for heating, see [activePhases response](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/blob/main/API.md?ref_type=heads#set-active-phases)    
**home/acjCtrl/pwrCtrl/controlParams** the parameters related to the PID control argorithm, see [controlParams response](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/blob/main/API.md?ref_type=heads#temperature-control-parameters)  
**home/acjCtrl/system/status** some parameters related to memory usage, see the [systemStatus response](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/blob/main/API.md?ref_type=heads#system-status)  

### subscribing from
**home/acjCtrl/pwrCtrl/setpoint/set** the new temperature setpoint to be used \[deg C]
**home/acjCtrl/pwrCtrl/reportRate/set** how often [tempCtrlStatus response](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/blob/main/API.md?ref_type=heads#temperature-control-report) are being sent automatically \[loop iterations]  
**home/acjCtrl/pwrCtrl/controlParams/set** updates the PID parameters, see [setControlParams command](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/blob/main/API.md?ref_type=heads#temperature-control-parameters)  
**home/acjCtrl/pwrCtrl/controlParams/req** requests a new response to be sent for [controlParams](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/blob/main/API.md?ref_type=heads#temperature-control-parameters)  
**home/acjCtrl/pwrCtrl/activePhases/set** makes it possible to disable one phase, see [activePhases command](home/acjCtrl/pwrCtrl/activePhases/set)  
**home/acjCtrl/system/status/req** requests a new response for [systemStatus](https://gitlab.com/acj-ctrl/sw/arduino-micro-shield-fw/-/blob/main/API.md?ref_type=heads#system-status)  

## License
This project is distributed under the MIT license. See the LICENSE file in this project for a more detailed description.

## Project status
Works for me but still under development. The MQTT interface (topics) is not yet decided and will probably be changed in upcoming versions until v1.0 is released.
